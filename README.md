# LMSR

This is our proposed LMSR model for the AIM 2020 efficient Super-Resolution challenge.

## Getting Started

### Installing

For the MTLU package Cuda version 10 needs to be installed.
Afterwards the MTLU package can be installed by doing a `pip install .` in `models/MTLU_Package`, where the `setup.py` is located

### Training

For training, one has to modify the option file (`options/train/lmsr.yml`) to use the right paths to the train and validation images.
After that, `train.py -opt options/train/lmsr.yml` can be executed to start training.

### Testing

Our pretrained model is in `challenge/LMSR.pth` and with `test_LMSR.py` the model can be tested. The testset paths and the model paths in this file need to be adapted.

