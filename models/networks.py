import torch
import models.archs.SRResNet_arch as SRResNet_arch



# Generator
def define_G(opt):
    opt_net = opt['network_G']
    which_model = opt_net['which_model_G']

    # image restoration
    if which_model == 'MSRResNet':
        netG = SRResNet_arch.MSRResNet(in_nc=opt_net['in_nc'], out_nc=opt_net['out_nc'],
                                       nf=opt_net['nf'], nb=opt_net['nb'], upscale=opt_net['scale'],
                                       basic_block=opt_net['basic_block'], upsampling=opt_net['upsampling'])
    elif which_model == 'SRResNet':
        netG = SRResNet_arch.SRResNet(in_nc=opt_net['in_nc'], out_nc=opt_net['out_nc'],
                                      nf=opt_net['nf'], nb=opt_net['nb'], upscale=opt_net['scale'],
                                      basic_block=opt_net['basic_block'])
    else:
        raise NotImplementedError('Generator model [{:s}] not recognized'.format(which_model))

    return netG



