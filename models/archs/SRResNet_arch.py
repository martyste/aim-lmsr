import functools
import torch.nn as nn
import torch.nn.functional as F
import models.archs.arch_util as arch_util
import torch.nn.functional as F
from models.MTLU_Package.MTLU import MTLU
import torch



class MSRResNet(nn.Module):
    ''' modified SRResNet'''

    def __init__(self, in_nc=3, out_nc=3, nf=64, nb=16, upscale=4, basic_block='no_bn', upsampling='normal'):
        super(MSRResNet, self).__init__()
        self.upscale = upscale

        self.conv_first = nn.Conv2d(in_nc, nf, 3, 1, 1, bias=True)

        # ADDED: Different basic blocks
        print(basic_block)
        assert (basic_block in ['no_bn', 'bn', 'mtlu', 'mtlu_with_relu'])
        if basic_block == 'no_bn':
            basic_block = functools.partial(arch_util.ResidualBlock_noBN, nf=nf)
        elif basic_block == 'mtlu':
            basic_block = functools.partial(arch_util.ResidualBlock_withBN_mtlu, nf=nf)
        elif basic_block == 'bn':
            basic_block = functools.partial(arch_util.ResidualBlock_withBN, nf=nf)
        elif basic_block == 'mtlu_with_relu':
            basic_block = functools.partial(arch_util.ResidualBlock_withBN_mtlu_relu, nf=nf)

        self.recon_trunk = arch_util.make_layer(basic_block, nb)

        # upsampling
        if self.upscale == 2:
            self.upconv1 = nn.Conv2d(nf, nf * 4, 3, 1, 1, bias=True)
            self.pixel_shuffle = nn.PixelShuffle(2)
        elif self.upscale == 3:
            self.upconv1 = nn.Conv2d(nf, nf * 9, 3, 1, 1, bias=True)
            self.pixel_shuffle = nn.PixelShuffle(3)
        elif self.upscale == 4:
            #ADDED: DIfferent upsampling methods
            print(upsampling)
            assert (upsampling in ['normal','small'])
            if upsampling == 'normal':
                self.upconv1 = nn.Conv2d(nf, nf * 4, 3, 1, 1, bias=True)
                self.upconv2 = nn.Conv2d(nf, nf * 4, 3, 1, 1, bias=True)
            elif upsampling == 'small':
                self.upconv1 = nn.Conv2d(nf, nf * 2, 3, 1, 1, bias=True)
                self.upconv2 = nn.Conv2d(nf // 2, nf, 3, 1, 1, bias=True)
            self.pixel_shuffle = nn.PixelShuffle(2)

        if upsampling == 'normal':
            self.HRconv = nn.Conv2d(nf, nf, 3, 1, 1, bias=True)
            self.conv_last = nn.Conv2d(nf, out_nc, 3, 1, 1, bias=True)
        elif upsampling == 'small':
            self.HRconv = nn.Conv2d(nf // 4, nf // 4, 3, 1, 1, bias=True)
            self.conv_last = nn.Conv2d(nf // 4, out_nc, 3, 1, 1, bias=True)


        # activation function
        self.lrelu = nn.LeakyReLU(negative_slope=0.1, inplace=True)

        # initialization
        arch_util.initialize_weights([self.conv_first, self.upconv1, self.HRconv, self.conv_last],
                                     0.1)
        if self.upscale == 4:
            arch_util.initialize_weights(self.upconv2, 0.1)

    def forward(self, x):
        fea = self.lrelu(self.conv_first(x))
        out = self.recon_trunk(fea)

        if self.upscale == 4:
            out = self.lrelu(self.pixel_shuffle(self.upconv1(out)))
            out = self.lrelu(self.pixel_shuffle(self.upconv2(out)))
        elif self.upscale == 3 or self.upscale == 2:
            out = self.lrelu(self.pixel_shuffle(self.upconv1(out)))

        out = self.conv_last(self.lrelu(self.HRconv(out)))
        base = F.interpolate(x, scale_factor=self.upscale, mode='bilinear', align_corners=False)
        out += base
        return out

